# Flask shop

### 1. How to setup

Download source code: 
```shell
$ git clone git@gitlab.com:K1ick/ip-shop-python-flask.git
```
Install dependent packages:
```shell
$ cd ip-shop-python-flask
$ python -m virtualenv env
$ source env/bin/activate
$ pip3 install -r requirement.txt
```


Run it:
```shell
$ python3 app.py
```
("database.py" is used to initialize the database.

"database.db" is stored with some of uploaded information.)

Access locally :
```
http://localhost:5000
```

